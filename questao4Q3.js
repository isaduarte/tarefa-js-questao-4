import gods from "./arquivo_exercicio_4.js";

function compare(a, b) {
    if (a.pantheon < b.pantheon) {
        return -1;
    } 
    else if (a.pantheon > b.pantheon) {
        return 1;
    } 
    else {
        return 0;
    }
}

function imprimirobjeto() {
    var num = 0;
    for (let i = 0; i < gods.length; i++) {
        console.log(num, gods[i]);
        num++
    }
}

gods.sort(compare);
imprimirobjeto();